//
//  KiedyPrawko.swift
//  kiedy-prawko-swift
//
//  Created by Patryk Mieszała on 22.05.2018.
//

import SwiftSoup
import Alamofire
import Foundation

public final class KiedyPrawko {
    
    var url: URL {
        return URL(string: "https://info-car.pl/infocar/prawo-jazdy/sprawdz-status.html?_hn:type=action&_hn:ref=r7_r1_r2_r1")!
    }
    
    var parameters: Parameters {
        return [
            "pesel": "81800400538518381114",
            "firstName": "Patryk",
            "lastName": "Mieszała"
        ]
    }
    
    private var timer: Timer?
    private var lastState: PrawkoState?
    
    private let notify: Notify
    
    private let session: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.urlCache = nil
        
        return SessionManager(configuration: configuration)
    }()
    
    enum Error: Swift.Error {
        case invalidArgumentsNumber
        case invalidArguments
    }
    
    public init() throws {
        let arguments = CommandLine.arguments
        guard arguments.count == 3 else {
            throw Error.invalidArgumentsNumber
        }
        
        guard let option = Option(rawValue: arguments[1], value: arguments[2]) else {
                throw Error.invalidArguments
        }

        self.notify = Notify(to: option.value)
    }
    
    public func run() {
        getHTML()
        
        RunLoop.main.run()
    }
    
    @objc private func getHTML() {
        timer?.invalidate()
        timer = nil
        
        session
            .request(url, method: .post, parameters: parameters, encoding: URLEncoding(), headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseString { (response) in
                switch response.result {
                case .success(let string):
                    do {
                        try self.handle(string: string)
                    }
                    catch let error {
                        self.notify.error(error: error)
                    }
                    
                case .failure(let error):
                    self.notify.error(error: error)
                }
                
                self.scheduleTimer(timeInterval: 60 * 5)
        }
    }
    
    private func scheduleTimer(timeInterval: Double) {
        self.timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(KiedyPrawko.getHTML), userInfo: nil, repeats: false)
    }
    
    func handle(string: String) throws {
        let doc = try SwiftSoup.parse(string)
        let selected = try doc.getElementsByClass("state active")
        
        guard let span = try selected.first()?.getElementsByTag("span").first()?.ownText() else {
            throw HTMLError.couldNotGetStringFromStateActive(string)
        }
        
        guard let state = PrawkoState(rawValue: span) else {
            throw HTMLError.couldNotGetStateFromSpan(string)
        }
        
        if self.lastState != state {
            notify.stateChanged(to: state)
        }
        
        self.lastState = state
    }
}
