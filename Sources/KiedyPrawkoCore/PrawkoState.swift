//
//  PrawkoState.swift
//  KiedyPrawko
//
//  Created by Patryk Mieszała on 22.05.2018.
//

enum PrawkoState: String {
    
    case processing = "Przyjęto wniosek, trwa postępowanie administracyjne"
    case ordered = "Dokument został zamówiony"
    case readyToPickUp = "Dokument do odbioru w urzędzie"
    case released = "Dokument wydany"
    
    var text: String {
        switch self {
        case .readyToPickUp: return "!!!! DO ODBIORU KURWA !!!!"
        default: return rawValue
        }
    }
    
    var title: String {
        switch self {
        case .readyToPickUp: return "!!!! DO ODBIORU KURWA !!!!"
        default: return "Kiedy Prawko"
        }
    }
    
    var subtitle: String {
        switch self {
        case .readyToPickUp: return "!!!! DO ODBIORU KURWA !!!!"
        default: return "Status"
        }
    }
}
