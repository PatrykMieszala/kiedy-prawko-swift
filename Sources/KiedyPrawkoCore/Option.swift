//
//  OptionType.swift
//  Alamofire
//
//  Created by Patryk Mieszała on 22.05.2018.
//

class Option {
    
    enum OptionType: String {
        case to = "-t"
    }
    
    let type: OptionType
    let value: String
    
    init?(rawValue: String, value: String) {
        guard let type = OptionType(rawValue: "-t") else { return nil }
        
        self.type = type
        self.value = value
    }
}
