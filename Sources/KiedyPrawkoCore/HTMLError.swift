//
//  HTMLError.swift
//  KiedyPrawko
//
//  Created by Patryk Mieszała on 22.05.2018.
//

enum HTMLError: Error, CustomStringConvertible {
    case couldNotGetStringFromStateActive(String)
    case couldNotGetStateFromSpan(String)
    
    var description: String {
        switch self {
        case .couldNotGetStateFromSpan: return "couldNotGetStateFromSpan"
        case .couldNotGetStringFromStateActive: return "couldNotGetStringFromStateActive"
        }
    }
    
    var html: String {
        switch self {
        case .couldNotGetStateFromSpan(let html): return html
        case .couldNotGetStringFromStateActive(let html): return html
        }
    }
}
