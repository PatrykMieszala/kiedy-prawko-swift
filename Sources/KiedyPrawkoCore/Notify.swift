//
//  NotifcationCenter.swift
//  KiedyPrawkoCore
//
//  Created by Patryk Mieszała on 22.05.2018.
//

import Foundation
import PerfectSMTP

class Notify {
    
    let hostname: String = "poczta.o2.pl"
    let email: String = "kiedyprawko@o2.pl"
    let password: String = "kiedyprawko1234"
    let toEmail: String
    
    lazy var client = SMTPClient(url: "smtps://" + hostname, username: email, password: password)
    
    lazy var recipient = Recipient(address: toEmail)
    
    var lastErrorDate: Date?
    
    init(to: String) {
        self.toEmail = to
    }
    
    func stateChanged(to state: PrawkoState) {
        print("state:", state)
        
        Process.launchedProcess(launchPath: "/usr/bin/osascript", arguments: ["-e", "display notification \"\(state.text)\" with title \"\(state.title)\" subtitle \"\(state.subtitle)\" sound name \"Glass\""])
        
        let email = EMail(client: client)
        email.subject = "Kiedy Prawko, status: \(state.text)"
        email.from = Recipient(address: self.email)
        email.to = [recipient]
        email.text = "Status: \(state.text)"
        
        do {
            try email.send()
        } catch let error {
            print(error)
            Process.launchedProcess(launchPath: "/usr/bin/osascript", arguments: ["-e", "display notification \"\(error)\" with title \"Kiedy Prawko\" subtitle \"Błąd\" sound name \"Glass\""])
        }
    }
    
    func error(error: Error) {
        print("error:", error)
        
        Process.launchedProcess(launchPath: "/usr/bin/osascript", arguments: ["-e", "display notification \"\(error)\" with title \"Kiedy Prawko\" subtitle \"Błąd\" sound name \"Glass\""])
        
        guard let date = lastErrorDate else {
            self.lastErrorDate = Date()
            
            return
        }
        
        guard
            let minute = Calendar
                .current
                .dateComponents([.minute], from: date, to: Date())
                .minute,
            minute >= 30
            else {
                return
        }
        
        let email = EMail(client: client)
        email.subject = "Kiedy Prawko błąd: \(error)"
        email.from = Recipient(address: self.email)
        email.to = [recipient]
        email.text = "Error: \(error)"
        
        if let error = error as? HTMLError {
            email.html = error.html
        }
        
        do {
            try email.send()
        } catch let error {
            print(error)
            Process.launchedProcess(launchPath: "/usr/bin/osascript", arguments: ["-e", "display notification \"\(error)\" with title \"Kiedy Prawko\" subtitle \"Błąd\" sound name \"Glass\""])
        }
        
        self.lastErrorDate = Date()
    }
}
