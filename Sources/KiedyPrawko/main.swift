import KiedyPrawkoCore

do {
    let kiedyPrawko = try KiedyPrawko()
    
    kiedyPrawko.run()
}
catch let error {
    print(error)
}
