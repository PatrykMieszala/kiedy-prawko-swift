// swift-tools-version:4.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "KiedyPrawko",
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/scinfu/SwiftSoup", from: "1.7.1"),
        .package(url: "https://github.com/Alamofire/Alamofire", from: "4.7.2"),
        .package(url: "https://github.com/PerfectlySoft/Perfect-SMTP.git", from: "3.1.1")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "KiedyPrawko",
            dependencies: ["KiedyPrawkoCore"]),
        .target(
            name: "KiedyPrawkoCore",
            dependencies: ["SwiftSoup", "Alamofire", "PerfectSMTP"]),
        ]
)
